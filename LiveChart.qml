import QtQuick 2.0
import QtQuick.Controls 2.0
import QtCharts 2.0
import QtQuick.Layouts 1.3

import "Globali"

Item {
    property real zoomPartenza: 1.0
    property real maxZoom: 1.4
    property int  countZoom: 1
    property int  numeroScroll :0 // mi indica quante volte ho scrollato mi serve x resettare la posizione
    property int lx: 1100

    ChartView {
        id: chart
        property var selectedPoint: undefined
        title: qsTr("Serie animate")+myTranslator.emptyString
        width: 1000
        height: 650
        anchors.verticalCenter: parent.verticalCenter
        backgroundColor: colori.verde
        backgroundRoundness: 10
        plotAreaColor: "#33826400"/*-D2DE00-*/
        //        anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalcenter
        anchors.left: parent.left
        anchors.leftMargin: 30
        antialiasing: true

        property real toleranceX: 0.05
        property real toleranceY: 0.05

        ValueAxis {
            id: axisX
            min: 0
            max: 10
            tickCount: 6
        }
        ValueAxis {
            id: axisY
            min: -0.5
            max: 1.5
            tickCount: 5
        }
        LineSeries {
            id: series1
            visible: checkBox.checked
            axisX: axisX
            axisY: axisY
            pointsVisible: true
            color: "black"
            width: 2
//            onClicked: console.log("onClicked: " + point.x + ", " + point.y);
        }
        LineSeries {
            id: series2
            visible: checkBox1.checked
            axisX: axisX
            axisY: axisY
            pointsVisible: true
            color: "red"
            width: 2
        }
        LineSeries {
            id: series3
            visible: checkBox2.checked
            axisX: axisX
            axisY: axisY
            pointsVisible: true
            color: "green"
            width: 2
        }
        LineSeries {
            id: series4
            visible: checkBox3.checked
            axisX: axisX
            axisY: axisY
            pointsVisible: true
            color: "blue"
            width: 2
        }
    } //end chart view

    // mousearea ancorata all' area del grafico
    MouseArea{
        x:chart.plotArea.x
        y:chart.plotArea.y
        width: chart.plotArea.width
        height: chart.plotArea.height
        hoverEnabled: true
        property real precX: 0
        property real precY:0
        onPressed: {
            // Salvo coordinate del mouse
            precX=mouseX
            precY=mouseY
        }
        onMouseXChanged: {
            if(pressed==true){ // xchè  ho messo hoverEnabled==true
                if(precX != mouseX) {

                    if(precX>(mouseX+chart.plotArea.width*0.05)) {                       //mouseX+20 x avere un po di margine e non scroll per ogni pixel serve anxhe x o scroll dello zoom sull'asse delle y
                        precX=mouseX
//                        console.log("mi muovo verso sinistra")
                        chart.scrollRight(chart.plotArea.width*0.10)
                        numeroScroll++
                    }
                    else if(precX<(mouseX-chart.plotArea.width*0.05)) {
                        precX=mouseX
//                        console.log("mi muovo verso destro")
                        chart.scrollLeft(chart.plotArea.width*0.10)
                        numeroScroll--
                    }
                }
            }
        }
        onMouseYChanged: {

            if(pressed==true){ // xchè  ho messo hoverEnabled==true
                if(precY != mouseY) {

                    if(precY>(mouseY+(chart.plotArea.height/7))) {
                        precY=mouseY
//                        console.log("SALGO")
                        if(countZoom<3){
                            countZoom++
                            chart.zoomIn()
                        }
                    }
                    else if(precY<(mouseY-(chart.plotArea.height/7))){
                        precY=mouseY
//                        console.log("SCENDO")

                        if(countZoom>-3){
                            countZoom--
                            chart.zoomOut()
                        }
                    }
                }
            }
        }
    }

    Column {
        //        anchors.left: chart.right
        //        anchors.leftMargin: 20
        anchors.verticalCenter: parent.verticalCenter
//                anchors.right: parent.right
//                anchors.rightMargin: 30
        spacing: 10

        BtnScroll {
            id: btn_scrollRight
            //            y: 30
            text: qsTr("destra --->")+myTranslator.emptyString
            onClicked: {
                chart.scrollRight(chart.plotArea.width*0.10)
                numeroScroll++
            }
        }
        BtnScroll {
            id: btn_scrollLeft
            //            y: 100
            text: qsTr("sinistra <---")+myTranslator.emptyString
            onClicked: {
                chart.scrollLeft(chart.plotArea.width*0.10)
                numeroScroll--
            }
        }
        BtnScroll {
            id: btn_resetScroll
            //            y: 330+height+10
            text: qsTr("reset")+myTranslator.emptyString
            onClicked: {
                // var point = chart.mapToValue(series1.at(series1.count-1))
                // console.log(point.x.toString())
                // chart.scrollRight(point.x)
//                console.log(numeroScroll.toString())
                if(numeroScroll>0)
                    chart.scrollRight(-(chart.plotArea.width*0.10*numeroScroll))
                else
                    chart.scrollRight(-(chart.plotArea.width*0.10*numeroScroll))
                numeroScroll=0    // resetto la variabile
            }
        }
        BtnScroll {
            id: btn_zoomIn
            //            y: 170
            text: qsTr("zoom +")+myTranslator.emptyString
            enabled: countZoom <3? true: false//zoomPartenza>=1.4? false : true
            onClicked: {
                if(countZoom<3){
                    countZoom++
                    chart.zoomIn()
                }
            }
        }
        BtnScroll {
            id: btn_zoomOut
            //            y: 240
            enabled: countZoom>-3? true : false //zoomPartenza<=0.5 ? false : true
            text: qsTr("zoom -")+myTranslator.emptyString
            onClicked: {
                if(countZoom>-3){
                    countZoom--
                    chart.zoomOut()
                }
            }
        }
        BtnScroll {
            id: btn_resetZoom
            //            y: 340
            text: qsTr("zoom 0")+myTranslator.emptyString
            onClicked: {
                if(countZoom!=1){

                    if(countZoom>1) {
                        for(var i=countZoom; i>1;i--)
                            chart.zoomOut()
                    }
                    else {

                        for(var i=countZoom; i<1;i++)
                            chart.zoomIn()
                    }
                    countZoom=1
                }
            }
        }

        CheckBox {
            id: checkBox
            x: lx
            //            y: 364
            spacing: 10
            Text {
                text: qsTr("Serie 1")+myTranslator.emptyString
                color: "black"
                font.pixelSize: 20
                anchors.left: parent.right
                verticalAlignment: Text.AlignVCenter
            }
        }
        CheckBox {
            id: checkBox1
            x: lx
            //            y: 503
            spacing: 10
            Text {
                text: qsTr("Serie 2")+myTranslator.emptyString
                color: "red"
                font.pixelSize: 20
                anchors.left: parent.right
                verticalAlignment: Text.AlignVCenter
            }
        }
        CheckBox {
            id: checkBox2
            x: lx
            //            y: 585
            spacing: 10
            Text {
                text: qsTr("Serie 3")+myTranslator.emptyString
                color: "green"
                font.pixelSize: 20
                anchors.left: parent.right
                verticalAlignment: Text.AlignVCenter
            }
        }
        CheckBox {
            id: checkBox3
            x: lx
            //            y: 667
            spacing: 10
            Text {
                text: qsTr("Serie 4")+myTranslator.emptyString
                color: "blue"
                font.pixelSize: 20
                anchors.left: parent.right
                verticalAlignment: Text.AlignVCenter
            }
        }
    }// end bottoni
    // il timer fa saltare l'editor visuale se si usa l'editor va commentato
    Timer {
        id: tmrToAddPoint
        property int i: 1
        running: true
        repeat: true
        interval: 1000
        onTriggered:{
            series1.append(i, Math.random()) ;
            series2.append(i, Math.random()) ;
            series3.append(i, Math.random()) ;
            series4.append(i, Math.random()) ;
            i++
            if(i>10){
                // do effetto di live chart lo scrool dipende dal livello di zoom
                switch(countZoom){
                case -3:chart.scrollRight(chart.plotArea.width*0.00625); break;  // zoom con fattore 16
                case -2:chart.scrollRight(chart.plotArea.width*0.0125); break;   // zoom con fattore 8
                case -1:chart.scrollRight(chart.plotArea.width*0.025); break;    // zoom con fattore 4
                case 0: chart.scrollRight(chart.plotArea.width*0.05); break;     // zoom con fattore 2
                case 1: chart.scrollRight(chart.plotArea.width*0.10); break;     // zoom con fattore 1
                case 2:  chart.scrollRight(chart.plotArea.width*0.20); break;    // zoom con fattore 0.5
                case 3: chart.scrollRight(chart.plotArea.width*0.40); break;     // zoom con fattore 0.25
                }
            }
        }
    }
}
