import QtQuick 2.9
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Flat 1.0
import QtQuick.Controls.Private 1.0
import QtQuick.Extras.Private 1.0
import QtQuick.Extras.Private.CppUtils 1.1

import "Globali"

Item {
    id : root

    /*-- GLOBALI --*/
    Caratteri {
        id: caratteri
    }
    Colori {
        id: colori
    }
    property int xl: 580
    property int yl: 600
    property int margineFinestra: 30
    property int margineSotto: 140
    property int margini: 10
    property int radio: 15
    /*-- __ --*/

    Rectangle {
        anchors.bottom: root.bottom
        anchors.bottomMargin: 30

        width: 120
        height: 60

        color: colori.verde
        border.color: colori.grigio
        border.width: 2
        anchors.horizontalCenter: root.horizontalCenter

        radius: radio
        id: contieneSalva

        Text {
            id: testoSalva
            text: qsTr("SALVA")+myTranslator.emptyString
            font.pixelSize: 22
            color: "white"
            font.family: caratteri.kl_Bold
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }

        MouseArea {
            anchors.fill: parent

            onClicked: {
                myData.setMese(Qt.formatDate(calendar1.selectedDate, "MM"));
                myData.setDay(Qt.formatDate(calendar1.selectedDate, "dd"));
                myData.setYear(Qt.formatDateTime(calendar1.selectedDate, "yyyy"));

                if (hoursColumn.currentIndex < 10)
                    myData.setHours("0"+hoursColumn.currentIndex)
                else
                    myData.setHours(""+hoursColumn.currentIndex)

                if (minuteColumn.currentIndex < 10)
                    myData.setMin("0"+minuteColumn.currentIndex)
                else
                    myData.setMin(""+minuteColumn.currentIndex)

                myData.setDateHours();
            }
            onPressed: {
                contieneSalva.width = 100
                contieneSalva.height = 40
                contieneSalva.anchors.bottomMargin = 40
                testoSalva.color = colori.grigio
            }
            onReleased: {
                contieneSalva.width = 120
                contieneSalva.height = 60
                contieneSalva.anchors.bottomMargin = 30
                testoSalva.color = "white"
            }
        }
    }
    Rectangle{
        id : containerTumbler
        anchors.top: parent.top
        anchors.topMargin: margineFinestra
        anchors.left: root.left
        anchors.leftMargin: margineFinestra
        radius: radio
        height: yl
        width: xl
        color: colori.verde

        Rectangle {
            id: title
            color: colori.grigio
            radius: radio
            anchors.top: parent.top
            height: 40
            width: parent.width

            Rectangle { //fittizzio
                width: parent.width
                anchors.bottom: parent.bottom
                height: 20
                color: parent.color
            }
            Text {
                id: txtSettOra
                anchors.fill: parent
                text: qsTr("IMPOSTA ORA")+myTranslator.emptyString
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 20
                color: "white"
                font.family: caratteri.kl_Regular
            }
        }
        Rectangle{
            id:rectfooter
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom:  parent.bottom
            anchors.bottomMargin: 0
            height: 70
            color: colori.grigio
            radius: radio
            Rectangle { //fittizzio
                width: parent.width
                anchors.top: parent.top
                height: 30
                color: parent.color
            }
        }
        Tumbler {
            id: tumbler
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top:title.bottom
            anchors.bottom: rectfooter.top
            height: 300

            TumblerColumn {
                id: hoursColumn
                width: 140
                model: 24 /*["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]*/
                highlight: Item{
                    width: 130
                    height: 65
                    y:hoursColumn.height/2

                    Rectangle {
                        id: linetop
                        width: parent.width
                        height: parent.height
                        radius: radio
                        border.color: colori.verde
                        border.width: 2
                        color: colori.grigio
                    }
                }
            }
            TumblerColumn {
                width:140
                id: minuteColumn
                model: 60

                highlight: Item{
                    width: 130
                    height: 65
                    y:minuteColumn.height/2

                    Rectangle {
                        id:linetop2
                        width: parent.width
                        height: parent.height
                        radius: radio
                        border.color: colori.verde
                        border.width: 2
                        color: colori.grigio
                    }
                }
            }

            style: TumblerStyle {
                id: tumblerStyle

                delegate: Item {
                    implicitHeight: (tumbler.height - padding.top - padding.bottom) / tumblerStyle.visibleItemCount

                    Text {
                        id: label
                        text: styleData.value
                        color: "white"
                        anchors.centerIn: parent
                        font.pixelSize: 30
                        font.family:caratteri.kl_Regular
                    }
                }

                property Component frame: Item {}
                property Component separator: Item {
                    id:sep
                    implicitWidth: 50 //tumbler.width
                    implicitHeight: tumbler.height
                    Rectangle {
                        width: 50
                        height: tumbler.height
                        color: "#00000000"
                    }
                }
            }
        }
    }

    Rectangle {
        id : containerCalendar
        anchors.verticalCenter: containerTumbler.verticalCenter
        anchors.right: root.right
        anchors.rightMargin: margineFinestra
        anchors.top: root.top
        anchors.topMargin: margineFinestra
        radius: radio
        height: yl
        width: xl
        color: colori.verde

        Rectangle{
            id:titlec
            color: colori.grigio
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: 40
            radius: radio
            Rectangle {
                width: parent.width
                anchors.bottom: parent.bottom
                height: 20
                color: parent.color
            }
            Text {
                id: txtSettDAta
                anchors.fill: parent
                text: qsTr("IMPOSTA DATA")+myTranslator.emptyString
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 20
                color: "white"
                font.family: caratteri.kl_Regular
            }
        }

        Rectangle{
            id:rectfooterc
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom:  parent.bottom
            anchors.bottomMargin: 0
            height: 70
            color:colori.grigio
            radius: radio

            Rectangle {
                width: parent.width
                anchors.top: parent.top
                height: 30
                color: parent.color
            }
        }

        Calendar {
            id:calendar1
            anchors.top:titlec.bottom
            anchors.bottom: rectfooterc.top

            height:270

            anchors.left: parent.left
            anchors.leftMargin: margini
            anchors.right: parent.right
            anchors.rightMargin: margini

            clip: true ;
            dayOfWeekFormat: 1
            selectedDate: varGlobali.data

            style: CalendarStyle {
                background: Rectangle{
                    color:"transparent"
                    anchors.fill: parent
                }
                navigationBar: Rectangle{
                    color: "white"
                    height: 60
                    width: calendar1.widthz

                    Rectangle {
                        id: previousMonth
                        width: control.width/7
                        height: control.height * 0.10
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left:  parent.left
                        opacity:  mAPrevMonth.pressed ? 0.5 : 1.0
                        Image {
                            id: leftarrow
                            source: "qrc:/Images/left-angle-arrow.png"
                            anchors.verticalCenter: parent.verticalCenter
                            antialiasing: true
                        }
                        MouseArea {
                            id:mAPrevMonth
                            anchors.fill: parent
                            onClicked: control.showPreviousMonth()
                        }
                    }

                    Label {
                        id: dateText
                        text: styleData.title
                        font.pixelSize: 35
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: previousMonth.right
                        anchors.leftMargin: 2
                        anchors.right: nextMonth.left
                        anchors.rightMargin: 2
                        font.family: caratteri.kl_Regular
                        color: colori.grigio
                        font.bold: false
                    }
                    Rectangle {
                        id: nextMonth
                        width: control.width/7
                        height: control.height * 0.10
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        opacity:  mANextMonth.pressed ?   0.5 : 1.0

                        Image {
                            id: rightarrow
                            source: "qrc:/Images/right-angle-arrow.png"
                            anchors.verticalCenter: parent.verticalCenter
                            antialiasing: true
                        }

                        MouseArea {
                            id:mANextMonth
                            anchors.fill: parent
                            onClicked: control.showNextMonth()
                        }
                    }
                }
                gridVisible: false
                gridColor : "transparent"
                dayDelegate: Rectangle {
                    color: styleData.selected ? colori.grigio : "transparent"
                    border.width:1
                    radius: radio
                    border.color: styleData.selected ? "white" : "transparent"
                    Label {
                        text: styleData.date.getDate()
                        font.pointSize: 15
                        font.family: caratteri.kl_Regular
                        anchors.centerIn: parent
                        color: styleData.selected ? "white" :(styleData.visibleMonth && styleData.valid ?   colori.grigio : "white")
                        font.bold: false
                    }
                }
                dayOfWeekDelegate: Rectangle {

                    width: control.height*0.10
                    height: control.height*0.10
                    color: gridVisible ?  colori.grigio : "transparent"

                    Label {
                        text: Qt.locale("it_IT").dayName(styleData.dayOfWeek, control.dayOfWeekFormat)
                        font.pixelSize: 15
                        anchors.centerIn: parent
                        font.family: caratteri.kl_Regular
                        color: colori.grigio
                        font.weight: Font.Bold
                        antialiasing: true
                        font.italic: false
                        font.capitalization: Font.AllUppercase
                    }
                }
            }

            Component.onCompleted: {
                calendar1.selectedDate=new Date();
            }
        } //end calendar
    }
}
