import QtQuick 2.9
import QtQuick.Controls 2.2

import "Globali"

Page {
    width: 1280
    height: 800

    title: qsTr("Home")+myTranslator.emptyString

    Image {
        source: "qrc:/Images/logo_micro_new.png"
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -50
        anchors.horizontalCenter: parent.horizontalCenter

        width: 700
        height: width
    }
}
