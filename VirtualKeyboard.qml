import QtQuick 2.9
import QtQuick.Controls 2.0
import QtQuick.VirtualKeyboard 2.1
import QtQuick.Layouts 1.3

import "Globali"

Item {
    anchors.fill: parent
    anchors.centerIn: parent

    property int dim: 195
    property int margine: 10
    property int cella: 220

    Caratteri {
        id: caratteri
    }
    Colori {
        id: colori
    }
    Barra {
        id: barra
    }

    GridView {
        id: gridView
        width: 1100
        height: 550

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -10

        snapMode: GridView.SnapToRow
        highlightRangeMode: GridView.ApplyRange
        boundsBehavior: Flickable.StopAtBounds

        clip: true
        flickableDirection: Flickable.VerticalFlick
        flow: GridView.FlowLeftToRight
        flickDeceleration: 1498
        maximumFlickVelocity: 2498

        cellHeight: cella
        cellWidth: cella

        delegate: Item {
            height: gridView.cellHeight
            width: gridView.cellWidth

            Rectangle {
                width: dim
                height: dim
                anchors.margins: margine
//              color: "#0D000000"
                color: colori.grigio
                anchors.horizontalCenter: parent.horizontalCenter

                Rectangle {
                    width: dim
                    height: dim
                    anchors.margins: margine
                    color: "transparent"
                    anchors.horizontalCenter: parent.horizontalCenter

                    Image {
                        anchors.fill: parent
                        source: pathRicetta
                        fillMode: Image.PreserveAspectFit
                    }
                    Rectangle {
                        // barra nome
                        height: parent.height * 0.25
                        width: parent.width
                        color: "#E6FFFFFF"
                        anchors.bottom: parent.bottom

                        Barra {
                            width: parent.width / 2
                            text: nomeRicetta
                            font.pixelSize: 20
                            minimumPixelSize: 2
                            anchors.left: parent.left
                            anchors.leftMargin: 5
                            horizontalAlignment: Text.AlignLeft
                        }
                        Barra {
                            width: 30
                            text: "\uf017"
                            font.pointSize: 10
                            minimumPointSize: 2
                            anchors.right: parent.right
                            anchors.rightMargin: 5
                            horizontalAlignment: Text.AlignRight
                        }
                        Barra {
                            width: 50
                            text: Qt.formatTime(durata, "hh:mm:ss")
                            font.pointSize: 20
                            color: colori.verde
                            minimumPointSize: 2
                            anchors.right: parent.children[1].left
                            anchors.rightMargin: 2
                            horizontalAlignment: Text.AlignRight
                        }
                    }
//                    MouseArea {
//                        anchors.fill: parent
//                        onClicked: stackView.push("qrc:/RicettaStop.qml", {
//                                                      "temp": temperatura,
//                                                      "ricUrl": pathRicetta,
//                                                      "nomeRic": nomeRicetta,
//                                                      "timer": new Date(durata),
//                                                      "potenza": pot,
//                                                      "potCieloPlatea": potCielPlat
//                          })
//                    }
                }
            }
        }
        model: myModelFiltrato
        ScrollBar.vertical: ScrollBar {
            id: scrollBar
            active: true
            width: 15
            anchors.top: gridView.top
            //anchors.left: gridView.right
            anchors.topMargin: 0
            y: gridView.y + 30

            contentItem: Rectangle {
                implicitWidth: scrollBar.width
                implicitHeight: 50
                radius: width / 2
                color: colori.verde
            }
        }
    } // end grid view

}
