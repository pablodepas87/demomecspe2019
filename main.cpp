#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QCursor>

#include "Risorse/filtro.h"
#include "Risorse/modelloricette.h"
#include "Risorse/dataora.hpp"
#include "Risorse/qmltranslator.h"
//#include "Risorse/translator.h"

//#include "poppler-qml/pdfModel.h"
//#include "poppler-qml/pageImageProvider.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    dataOra d;
    QCursor c;
    c.setShape(Qt::BlankCursor);
    app.setOverrideCursor(c);

    ModelloRicette modelloRicette;

    Filtro filtroSuModello;
    filtroSuModello.setSourceModel(&modelloRicette);

//    filtroSuModello.setFilterRole(mioModello.NomeRicetta);
//    qmlRegisterType<PdfModel>("org.docviewer.poppler", 1, 0, "Poppler");

    engine.rootContext()->setContextProperty("myData", &d);
    engine.rootContext()->setContextProperty("myModelFiltrato",&filtroSuModello);
    engine.rootContext()->setContextProperty("modelloRicette",&modelloRicette);

    //translator
    //Translator::myTranslator = new Translator();
    QmlTranslator *myTranslator = new QmlTranslator();
    engine.rootContext()->setContextProperty("myTranslator", myTranslator);

    //translator

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
