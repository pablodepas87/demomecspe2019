import QtQuick 2.0
import QtQuick 2.9
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

import "Globali"

Item {
//    anchors.fill: parent
    id: tutto

    Caratteri{
        id: caratteri
    }
    Colori{
        id: colori
    }
    property int dim: 400
    property int margine: 150
    property int fontSz: 35

    Rectangle {
        anchors.left: tutto.left
        anchors.leftMargin: margine
        width: dim
        height: dim
        anchors.verticalCenter: tutto.verticalCenter

        MouseArea {
            anchors.fill: parent
             onClicked: {
                 stackView.push("qrc:/Charts.ui.qml")
             }
        }
        Column {
            anchors.fill: parent
            anchors.centerIn: parent
            Rectangle {
                width: parent.width
                height: 150
                color: colori.grigio
                radius: 15
                IconaGrafico {}
                Rectangle {
                    width: parent.width
                    anchors.bottom: parent.bottom
                    height: 50
                    color: colori.grigio
                }
            }
            Rectangle {
                width: parent.width
                color: colori.verde
                height: 300
                radius: 15
                Rectangle {
                    width: parent.width
                    anchors.top: parent.top
                    height: 50
                    color: colori.verde
                }
                Text {
                    text: qsTr("Grafici")+myTranslator.emptyString
                    color: "white"
                    font.pixelSize: fontSz
                    anchors.centerIn: parent
                    font.family: caratteri.kl_Regular
                }
            }
        }
    }
    Rectangle {
        anchors.right: tutto.right
        anchors.rightMargin: margine
        anchors.verticalCenter: tutto.verticalCenter
        width: dim
        height: dim/*-plot area background-*/

        MouseArea {
            anchors.fill: parent
             onClicked: {
                 stackView.push("qrc:/LiveChart.qml")
             }
        }
        Column  {
            anchors.fill: parent
            anchors.centerIn: parent
            Rectangle {
                width: parent.width
                height: 150
                color: colori.grigio
                radius: 15
                IconaGrafico {}
                Rectangle {
                    width: parent.width
                    anchors.bottom: parent.bottom
                    height:50
                    color: colori.grigio
                }
            }
            Rectangle {
                width: parent.width
                color: colori.verde
                height: 300
                radius: 15
                Rectangle {
                    width: parent.width
                    anchors.top: parent.top
                    height: 50
                    color: colori.verde
                }
                Text {
                    text: qsTr("Grafici animati")+myTranslator.emptyString
                    color: "white"
                    font.family: caratteri.kl_Regular
                    font.pixelSize: fontSz
                    anchors.centerIn: parent
                }
            }
        }
    }
}
