import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.VirtualKeyboard 2.1

import "Globali"

Page {
    id: page

    width:800
    height: 1280

    header: Rectangle{
        height: 70
        width: parent.width
        color: palette.line


        Text{
            x: 131
            y: 0

            visible: menu.visible==true? false : true
            text:qsTr("Lista Ricette")+myTranslator.emptyString
            font.pointSize: 25
            minimumPointSize: 2
            fontSizeMode: Text.Fit
            font.family: cookie.name
            color: palette.white

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            width:262
            height: 70
        }

        Text{
            x: 0
            y: 0
            visible: menu.visible==true? false : true
            text:"\uf0c9"
            font.pointSize: 20
            minimumPointSize: 2
            fontSizeMode: Text.Fit
            font.family: fontAwesome.name
            color: palette.white
            anchors.left: parent.left
            anchors.leftMargin: 10

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            width:70
            height: 70

            MouseArea{
                id: mArea
                anchors.fill: parent
                onClicked:{
                    menu.open()
                }
            }
        }
    }
    background: Rectangle{
        color:"#ebebeb"
    }

    TextField {
        id:textField
        anchors.top: parent.top ;anchors.topMargin: 10;
        anchors.horizontalCenter: parent.horizontalCenter
        placeholderText: qsTr("Cerca Ricetta")+myTranslator.emptyString
        width:parent.width*0.95
        font.pointSize: 12
        background: Rectangle {
            radius: 5
            width: parent.width
            implicitHeight: 30
        }

        onTextChanged: {
            //myModelFiltrato.setStringaFiltro(text)
            myModelFiltrato.string=text
        }
    }

    GridView {
        id: gridView

        width: 440
        height: 440
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        snapMode: GridView.SnapToRow
        highlightRangeMode: GridView.ApplyRange
        boundsBehavior: Flickable.StopAtBounds
        clip: true
        flickableDirection: Flickable.VerticalFlick
        flow: GridView.FlowLeftToRight
        flickDeceleration: 1498
        maximumFlickVelocity: 2498

        cellHeight: 220
        cellWidth: 220
        delegate: Item {
            height: gridView.cellHeight
            width:  gridView.cellWidth

            Rectangle{
                width: 212
                height: 212
                color: "#0D000000"
                anchors.horizontalCenter: parent.horizontalCenter

                Rectangle {
                    width: 210
                    height:210
                    color: "transparent"
                    anchors.horizontalCenter: parent.horizontalCenter

                    Image{
                        anchors.fill: parent
                        source: pathRicetta
                        fillMode: Image.PreserveAspectFit
                    }

                    Rectangle{
                        // barra nome
                        height: parent.height*0.25
                        width: parent.width
                        color: "#E6FFFFFF"
                        anchors.bottom: parent.bottom

                        Text{
                            width: parent.width/2
                            height: parent.height

                            text:nomeRicetta
                            font.pixelSize: 20
                            minimumPixelSize:  2
                            fontSizeMode: Text.Fit
                            font.family: cookie.name
                            color:  palette.line
                            anchors.left: parent.left
                            anchors.leftMargin: 5
                            horizontalAlignment: Text.AlignLeft
                            verticalAlignment: Text.AlignVCenter
                        }

                        Text{
                            width: 30
                            height: parent.height

                            text:"\uf017"
                            font.pointSize: 10
                            minimumPointSize: 2
                            fontSizeMode: Text.Fit
                            font.family: fontAwesome.name
                            color:  palette.line
                            anchors.right: parent.right
                            anchors.rightMargin:  5
                            horizontalAlignment: Text.AlignRight
                            verticalAlignment: Text.AlignVCenter
                        }
                        Text{
                            width: 50
                            height: parent.height
                            text:Qt.formatTime(durata,"hh:mm:ss")
                            font.pointSize: 20
                            minimumPointSize: 2
                            fontSizeMode: Text.Fit
                            font.family: cookie.name
                            color:  palette.line
                            anchors.right: parent.children[1].left
                            anchors.rightMargin:  2
                            horizontalAlignment: Text.AlignRight
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                }
            }
        }
        model: myModelFiltrato
        ScrollBar.vertical: ScrollBar {
            id:scrollBar
            active: true;
            width:15
            anchors.top:gridView.top
            //anchors.left: gridView.right
            anchors.topMargin: 0
            y:gridView.y+30

            contentItem: Rectangle {
                implicitWidth: scrollBar.width
                implicitHeight: 50
                radius: width / 2
                color: scrollBar.pressed ? "#802d86c7" : palette.line
            }
        }
    }

    Slider {
        id: control
        x: 37
        y: 538
        width: 400
        stepSize: 1
        value: 0
        visible: false
        from:0
        to:listView.count-1
        onValueChanged: listView.currentIndex=value
        onPositionChanged: {

            listView.currentIndex= valueAt(position)

        }
        snapMode:Slider.SnapAlways  // fa cambiare il value anche mentre sn in fase di pressed
        background: Rectangle {
            x: control.leftPadding
            y: control.topPadding + control.availableHeight / 2 - height / 2
            implicitWidth: 400
            implicitHeight: 4
            width: control.availableWidth
            height: implicitHeight
            radius: 2
            color: "#bdbebf"

            Rectangle {
                width: control.visualPosition * parent.width
                height: parent.height
                color: palette.line
                radius: 2
            }
        }
        handle: Rectangle {
            x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
            y: control.topPadding + control.availableHeight / 2 - height / 2
            implicitWidth: 26
            implicitHeight: 26
            radius: 13
            color: control.pressed ? "#f0f0f0" : "#f6f6f6"
            border.color: "#bdbebf"
        }
    }


    states: [
        State {
            name: "showList"

            PropertyChanges {
                target: gridView
                visible: false
            }

            PropertyChanges {
                target: listView
                visible:true

            }

            PropertyChanges {
                target: control
                visible:true
            }
        }
    ]



    footer: Item{

        height: 50
        width: parent.width



        Image {
            id: img
            source: page.state== "" ? "qrc:/Img/Img/immagine.png" : "qrc:/Img/Img/griglia.png"
            visible: inputPanel.state== "visible"? false : true
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.top: parent.top; anchors.topMargin:0
            scale:0.6
            MouseArea{
                id: mArea1
                anchors.fill: parent
                onClicked:{
                    if(page.state=="")
                        page.state= "showList"
                    else
                        page.state=""
                }
            }
        }


        Text{
            x: 0
            y: 0
            visible: false //inputPanel.state== "visible"? false : true
            text:"\uf0c9"
            font.pointSize: 20
            minimumPointSize: 2
            fontSizeMode: Text.Fit
            font.family: fontAwesome.name
            color: palette.line
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: parent.top; anchors.topMargin:0
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            width:50
            height: 50
        }
    }

    InputPanel {
        id: inputPanel
        z: 99

        y: page.height
        anchors.left: parent.left
        anchors.right: parent.right
        states: State {
            name: "visible"
            /*  The visibility of the InputPanel can be bound to the Qt.inputMethod.visible property,
              but then the handwriting input panel and the keyboard input panel can be visible
              at the same time. Here the visibility is bound to InputPanel.active property instead,
              which allows the handwriting panel to control the visibility when necessary.
          */
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: page.height - inputPanel.height-55
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }

    }


}
