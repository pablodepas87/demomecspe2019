import QtQuick 2.9
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.VirtualKeyboard 2.1

import "Globali"
import "Graphs"

ApplicationWindow {

    flags:Qt.SplashScreen
    /* --- OGGETTI IMPORTATI */
    Caratteri {
        id: caratteri
    }

    Colori {
        id: colori
    }
    /*--*/

    id: window
    visible: true
    width: 1280
    height: 800
    title: qsTr("Stack")

    header: ToolBar {
        height: 65

        background: Rectangle {
            color: colori.verde
        }
        ToolButton {
            id: toolButton
            height: parent.height
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"

            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                    toCalendario.enabled = true
                    topBarHome.text = qsTr("Home")
                } else {
                    menu.open()
                }
            }
        }
        Testo {
           id: topBarHome
           text: qsTr("Home")
           font.pixelSize: 30
           font.family: caratteri.kl_Bold
           color: "white"
           anchors.centerIn: parent
           anchors.fill: null
        }
        Text  {
            text: Qt.formatDateTime(myData.dataoraAttuale,"dd/MM/yyyy hh:mm")
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            height: parent.height
            anchors.right: parent.right
            anchors.rightMargin: 20
            font.pixelSize: 30
            width: 300
            color: "white"
            font.family: caratteri.kl_Bold

            MouseArea {
                id: toCalendario
                width: parent.width
                height: parent.height
                anchors.centerIn: parent

                onClicked: {
                    stackView.push("qrc:/Calendario.qml")
                    enabled=false
                }
            }
        }
    }

    Drawer {
        id: menu
        height: window.height
        width: 0.33 * window.width

        background: Rectangle{
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#FFFFFF"
                }
                GradientStop {
                    position: 0
                    color: "#FFD2DE00"
                }
            }
        }

        ColumnLayout {
            height: parent.height
            width: parent.width
            spacing: 0

            Item{
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredHeight: parent.height*0.70

                Item{
                    height: parent.height * 0.30
                    width: parent.width
                    id: immagine

                    Image{
                        source: "qrc:/Images/logo_micro_new.png"
                        width: parent.width * 0.45
                        height: width
                        anchors.centerIn: parent
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.horizontalCenterOffset: -20
                    }

                    Rectangle{ // --- BARRA
                        width: parent.width
                        height: 2
                        color: colori.verde
                        anchors.bottom: parent.bottom
                    }
                }

                ListView {
                    id: listView
                    clip:true
                    focus: true
                    interactive: false
                    width: parent.width
                    height: parent.height * 0.70
                    anchors.top: immagine.bottom


                    delegate: ItemDelegate {
                        id:item
                        height: listView.height / 6
                        width: listView.width
                        padding: 0

                        background: Item {
                            opacity: 0.80

                            Rectangle{ // --- BARRA
                                width: parent.width
                                height: 2
                                color: colori.verde
                                anchors.top: parent.bottom
                            }
                        }

                        contentItem: Item {
                            width: parent.width
                            height: parent.height

                            Text{
                                text:ico // --- ICO
                                font.pointSize: 20
                                font.family: caratteri.awesome
                                color: colori.verde

                                height: parent.height

                                leftPadding: 20
                                verticalAlignment: Text.AlignVCenter
                            }
                            Text{
                                text:qsTr(title)+myTranslator.emptyString// --- TITOLO
                                font.pixelSize:  26
                                color:colori.grigio
                                font.family: caratteri.kl_Bold
                                leftPadding: 65

                                height: parent.height
                                verticalAlignment: Text.AlignVCenter
                            }
                        } //end item
                        //text: model.title
                        highlighted: ListView.isCurrentItem

                        onClicked: {
                            listView.currentIndex = index
                            stackView.push(model.source)
                            menu.close()
                            topBarHome.text = model.title
                        }
                    } //end item delegate

                    model: ListModel {
                        ListElement { title: "Grafici"; ico:"\uf080"; source: "Page1Form.qml"; }
                        ListElement { title: "Tastiera virtuale";ico:"\uf11c"; source: "Page2Form.ui.qml" }
                        ListElement { title: "Pdf"; ico:"\uf1c1"; source: "Page4Form.ui.qml" }
                        ListElement { title: "Impostazioni"; ico:"\uf085"; source: "Page3Form.ui.qml" }
                    }
                    ScrollIndicator.vertical: ScrollIndicator { }
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "HomeForm.ui.qml"
        anchors.fill: parent
    }

    InputPanel {
        id: inputPanel
        width: parent.width
        y: parent.height
        z: 99

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: parent.height - inputPanel.height //c'era un -5 di troppo
            }
        }

        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true

            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }

    } //end input panel
}
