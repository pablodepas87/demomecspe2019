import QtQuick 2.0
import QtCharts 2.0
import QtQuick.Controls 2.0

Item {
    anchors.fill: parent

    property bool premuto: false
    property variant othersSlice: 0

    ChartView {
        id: chart
        title: qsTr("Top-5 marchi automobilistici")+myTranslator.emptyString
        legend.alignment: Qt.RightDockWidgetArea
        antialiasing: false
        legend.font.pixelSize: 14


        anchors.horizontalCenter: parent.horizontalcenter
        anchors.horizontalCenterOffset: 12
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset:  25

        width: parent.width
        height: 800

        PieSeries {
            id: pieSeries
            PieSlice { label: "Volkswagen"; value: 13.5 }
            PieSlice { label: "Toyota"; value: 10.9 }
            PieSlice { label: "Ford"; value: 8.6 }
            PieSlice { label: "Skoda"; value: 8.2 }
            PieSlice { label: "Volvo"; value: 6.8 }
        }
    }

    Switch {
        onCheckedChanged: {
            chart.antialiasing = !chart.antialiasing
        }
        text: qsTr("Antialiasing")+myTranslator.emptyString
        x: 100
        y:  50
    }

    Switch {
        onCheckedChanged: {
            pieSeries.find("Volkswagen").exploded = !pieSeries.find("Volkswagen").exploded;
        }
        text: qsTr("Stacca")+myTranslator.emptyString
        x: 100
        y: 100
    }

    Switch {
        x: 100
        y: 150

        text: qsTr("Aggiungi")+myTranslator.emptyString

        onCheckedChanged: {
            if (!premuto)
                othersSlice = pieSeries.append(qsTr("Altri"), 52.0);
            else
                pieSeries.remove(othersSlice);

            premuto = !premuto
        }
    }
}
