import QtQuick 2.9
import QtCharts 2.0
import QtQuick.Controls 2.2

Item {
    anchors.fill: parent
    id :tutto

    ChartView {
        title: qsTr("Scatter")+myTranslator.emptyString
        anchors.top: tutto.top
        anchors.horizontalCenter: tutto.horizontalCenter
        width: 1000
        height: 500
        antialiasing: true

        ScatterSeries {
            id: scatter1
            name: "Scatter1"
            XYPoint { x: 1.5; y: 1.5 }
            XYPoint { x: 1.5; y: 1.6 }
            XYPoint { x: 1.57; y: 1.55 }
            XYPoint { x: 1.8; y: 1.8 }
            XYPoint { x: 1.9; y: 1.6 }
            XYPoint { x: 2.1; y: 1.3 }
            XYPoint { x: 2.5; y: 2.1 }
        }

        ScatterSeries {
            visible: false
            id: scatter2
            name: "Scatter2"
            //![1]
            XYPoint { x: 1.82; y: 1.4 }
            XYPoint { x: 2.0; y: 1.5 }
            XYPoint { x: 2.07; y: 1.65 }
            XYPoint { x: 2.2; y: 1.7 }
            XYPoint { x: 2.3; y: 1.9 }
            XYPoint { x: 2.45; y: 2.05 }
        }
    }

    DelayButton {
        id: control
        checked: true
        text: qsTr("Mostra Scatter 2")+myTranslator.emptyString
        anchors.bottom: tutto.bottom
        anchors.horizontalCenter: tutto.horizontalCenter
        anchors.bottomMargin: 20

        contentItem: Text {
            text: control.text
            font: control.font
            opacity: enabled ? 1.0 : 0.3
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }

        onClicked: {
            scatter2.visible = !scatter2.visible
        }

        background: Rectangle {
            implicitWidth: 100
            implicitHeight: 100
            opacity: enabled ? 1 : 0.3
            color: control.down ? "#17a81a" : "#21be2b"
            radius: size / 2

            readonly property real size: Math.min(control.width, control.height)
            width: size
            height: size
            anchors.centerIn: parent

            Canvas {
                id: canvas
                anchors.fill: parent

                Connections {
                    target: control
                    onProgressChanged: canvas.requestPaint()
                }

                onPaint: {
                    var ctx = getContext("2d")
                    ctx.clearRect(0, 0, width, height)
                    ctx.strokeStyle = "white"
                    ctx.lineWidth = parent.size / 20
                    ctx.beginPath()
                    var startAngle = Math.PI / 5 * 3
                    var endAngle = startAngle + control.progress * Math.PI / 5 * 9
                    ctx.arc(width / 2, height / 2, width / 2 - ctx.lineWidth / 2 - 2, startAngle, endAngle)
                    ctx.stroke()
                }
            }
        }
    }
}
