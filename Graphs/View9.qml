import QtQuick 2.0
import QtCharts 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    anchors.fill: parent
    id: tutto

    ChartView {
        id: grafico
        title: "Grafico a barre orizzontali"
        anchors.top: tutto.top
        anchors.horizontalCenter: tutto.horizontalCenter
        width: 900
        height: 600

        legend.alignment: Qt.AlignBottom
        antialiasing: true

        HorizontalBarSeries {
            axisY: BarCategoryAxis { id: barre; categories: ["2013", "2014", "2015", "2016", "2017", "2018" ] }
            BarSet { id: bob; label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
            BarSet { id: susan; label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
            BarSet { id: james; label: "James"; values: [3, 5, 8, 13, 5, 8] }
        }
    }
    Frame {
        anchors.horizontalCenter: tutto.horizontalCenter
        anchors.bottom: tutto.bottom
        anchors.bottomMargin: 25
        anchors.topMargin: 10

        Row {
            RadioButton {
                text: "2013"
                onCheckedChanged: {
                    barre.categories = ["2014", "2015", "2016", "2017", "2018" ]
                    bob.values = [2, 3, 4, 5, 6]
                    susan.values = [1, 2, 4, 1, 7]
                    james.values = [5, 8, 13, 5, 8]
                }
            }
            RadioButton {
                text: "2014"

                onCheckedChanged: {
                    barre.categories = ["2013", "2015", "2016", "2017", "2018" ]
                    bob.values = [2, 3, 4, 5, 6]
                    susan.values = [5, 2, 4, 1, 7]
                    james.values = [3, 8, 13, 5, 8]
                }
            }
            RadioButton {
                text: "2015"

                onCheckedChanged: {
                    barre.categories = ["2013", "2014", "2016", "2017", "2018" ]
                    bob.values = [2, 2, 4, 5, 6]
                    susan.values = [5, 1, 4, 1, 7]
                    james.values = [3, 5, 13, 5, 8]
                }
            }
            RadioButton {
                text: "2016"

                onCheckedChanged: {
                    barre.categories = ["2013", "2014", "2015", "2017", "2018" ]
                    bob.values = [2, 2, 3, 5, 6]
                    susan.values = [5, 1, 2, 1, 7]
                    james.values = [3, 5, 8, 5, 8]
                }
            }
            RadioButton {
                text: "2017"

                onCheckedChanged: {
                    barre.categories = ["2013", "2014", "2015", "2016", "2018" ]
                    bob.values = [2, 2, 3, 4, 6]
                    susan.values = [5, 1, 2, 4, 7]
                    james.values = [3, 5, 8, 13, 8]
                }
            }
            RadioButton {
                text: "2018"

                onCheckedChanged: {
                    barre.categories = ["2013", "2014", "2015", "2016", "2017" ]
                    bob.values = [2, 2, 3, 4, 5]
                    susan.values = [5, 1, 2, 4, 1]
                    james.values = [3, 5, 8, 13, 5]
                }
            }
        }
    }
}
