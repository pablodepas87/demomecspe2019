import QtQuick 2.0
import QtCharts 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    anchors.fill: parent

        ChartView {

            id: grafico

            width: 500
            height: 600
            anchors.centerIn: parent

            title: qsTr("grafico a barre")+myTranslator.emptyString
            anchors.fill: parent
            antialiasing: true

            BarSeries {
                id: mySeries
//                axisX: BarCategoryAxis { categories: ["2007", "2008", "2009", "2010", "2011", "2012" ] }
                axisX: BarCategoryAxis {id: assex; categories: ["2013"] }
                BarSet { id: bob; label: "Bob"; values: [4] }
                BarSet { id: susan; label: "Susan"; values: [4] }
                BarSet { id: james; label: "James"; values: [13] }
            }
        }

        Slider {

            anchors.bottom: grafico.top
            anchors.horizontalCenter: grafico.horizontalCenter

            from: 2013
            value: 2013
            to: 2019
            stepSize: 1

            onValueChanged: {

                assex.categories = ["" + value]

                switch(value)
                {
                case 2013:
                    bob.values = [4]
                    susan.values = [4]
                    james.values = [13]
                    break;
                case 2014:
                    bob.values = [2]
                    susan.values = [1]
                    james.values = [5]
                    break;
                case 2015:
                    bob.values = [3]
                    susan.values = [2]
                    james.values = [8]
                    break;
                case 2016:
                    bob.values = [2]
                    susan.values = [5]
                    james.values = [3]
                    break;
                case 2017:
                    bob.values = [5]
                    susan.values = [1]
                    james.values = [5]
                    break;
                case 2018:
                    bob.values = [6]
                    susan.values = [7]
                    james.values = [8]
                    break;
                }

            }
        }
}
