﻿import QtQuick 2.0
import QtCharts 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    //    anchors.fill: parent
    id: tutto
    property string svedesi: "Bob"
    property string russi: "Susan"
    property string finlandesi: "James"
    property string rosso: qsTr("Rosso")+myTranslator.emptyString
    property string giallo: qsTr("Giallo")+myTranslator.emptyString
    property string verde: qsTr("Verde")+myTranslator.emptyString

    ChartView {
        title: qsTr("Grafico ad aree")+myTranslator.emptyString
//        anchors.top: tutto.top
        antialiasing: true
//        anchors.horizontalCenter: tutto.horizontalCenter
        width: 900
        height: 600
        //        anchors.left: tutto.left
//        anchors.leftMargin: 20
        id: grafico

        // Define x-axis to be used with the series instead of default one
        ValueAxis {
            id: valueAxis
            min: 2007
            max: 2018
            tickCount: 12
            labelFormat: "%.0f"
        }

//        Column {
//            anchors.verticalCenter: grafico.verticalCenter
//            spacing: 10

            AreaSeries {
                name: russi
                color: "yellow"
                axisX: valueAxis
                id: russia
                upperSeries: LineSeries {
                    XYPoint { x: 2007; y: 1 }
                    XYPoint { x: 2008; y: 1 }
                    XYPoint { x: 2009; y: 1 }
                    XYPoint { x: 2010; y: 1 }
                    XYPoint { x: 2011; y: 1 }
                    XYPoint { x: 2012; y: 0 }
                    XYPoint { x: 2013; y: 1 }
                    XYPoint { x: 2014; y: 1 }
                    XYPoint { x: 2015; y: 4 }
                    XYPoint { x: 2016; y: 3 }
                    XYPoint { x: 2017; y: 2 }
                    XYPoint { x: 2018; y: 1 }
                }
            }

            AreaSeries {
                name: svedesi
                color: "red"
                axisX: valueAxis
                id: svezia
                upperSeries: LineSeries {
                    XYPoint { x: 2007; y: 1 }
                    XYPoint { x: 2009; y: 1 }
                    XYPoint { x: 2009; y: 3 }
                    XYPoint { x: 2010; y: 3 }
                    XYPoint { x: 2011; y: 2 }
                    XYPoint { x: 2012; y: 0 }
                    XYPoint { x: 2013; y: 2 }
                    XYPoint { x: 2014; y: 1 }
                    XYPoint { x: 2015; y: 2 }
                    XYPoint { x: 2016; y: 1 }
                    XYPoint { x: 2017; y: 3 }
                    XYPoint { x: 2018; y: 3 }
                }
            }

            AreaSeries {
                name: finlandesi
                color: "green"
                axisX: valueAxis
                id: finlandia
                upperSeries: LineSeries {
                    XYPoint { x: 2007; y: 0 }
                    XYPoint { x: 2008; y: 0 }
                    XYPoint { x: 2009; y: 0 }
                    XYPoint { x: 2010; y: 0 }
                    XYPoint { x: 2011; y: 0 }
                    XYPoint { x: 2012; y: 0 }
                    XYPoint { x: 2013; y: 1 }
                    XYPoint { x: 2014; y: 0 }
                    XYPoint { x: 2015; y: 0 }
                    XYPoint { x: 2016; y: 0 }
                    XYPoint { x: 2017; y: 0 }
                    XYPoint { x: 2018; y: 1 }
                }
            }

//        }
    }

    Rectangle {

        anchors.left: grafico.right
        anchors.leftMargin: 20
        id: riga
        height: 600
        anchors.verticalCenter: grafico.verticalCenter
        width: 100

        Column {
            anchors.centerIn: parent
            anchors.fill: parent

            Text {
                text: russi
                font.pixelSize: 20
            }
            ComboBox {
                id: russiaBtn
                model: [giallo, rosso, verde]

                onCurrentIndexChanged: {
                    switch(currentIndex){
                    case 0:
                        russia.color = "yellow"
                        break;
                    case 1:
                        russia.color = "red"
                        break;
                    case 2:
                        russia.color = "green"
                        break;
                    }
                }
            }
            Text {
                text: svedesi
                font.pixelSize: 20
            }
            ComboBox {
                id: sveziaBtn
                model: [rosso, giallo, verde]

                onCurrentIndexChanged: {
                    switch(currentIndex){
                    case 0:
                        svezia.color = "red"
                        break;
                    case 1:
                        svezia.color = "yellow"
                        break;
                    case 2:
                        svezia.color = "green"
                        break;
                    }
                }
            }
            Text {
                text: finlandesi
                font.pixelSize: 20
            }
            ComboBox {
                id: finlandiaBtn
                model: [verde, rosso, giallo]

                onCurrentIndexChanged: {
                    switch(currentIndex){
                    case 0:
                        finlandia.color = "green"
                        break;
                    case 1:
                        finlandia.color = "red"
                        break;
                    case 2:
                        finlandia.color = "yellow"
                        break;
                    }
                }
            }
        }

    }

}
