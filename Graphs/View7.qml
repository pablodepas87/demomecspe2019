import QtQuick 2.0
import QtCharts 2.0

Item {
    anchors.fill: parent

    ChartView {
        title: qsTr("Grafico a barre orizzontali")+myTranslator.emptyString
        anchors.fill: parent
        legend.alignment: Qt.AlignBottom
        antialiasing: true

        StackedBarSeries {
            id: mySeries
            axisX: BarCategoryAxis { categories: ["2013", "2014", "2015", "2016", "2017", "2018" ] }
            BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
            BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
            BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
        }
    }
}
