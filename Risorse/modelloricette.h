#ifndef MODELLORICETTE_H
#define MODELLORICETTE_H

#include <QAbstractListModel>
#include <QTime>
#include <QDebug>

class Ricetta {

public:
    Ricetta(const QString &nome, const QString &urlImg, const QTime &durata, const qint16 temperatura,const qint16 potenza,const qint16 potCieloPlatea);

    QString nome() const;
    QString url() const;
    QTime   durata()const;
    qint16  temp()const;
    qint16 potenza() const;
    qint16 potCieloPlatea() const;

private:
    QString m_nome;
    QString m_url;
    QTime   m_durata;
    qint16  m_temp;
    qint16  m_potenza;
    qint16  m_potCieloPlatea;
};

class ModelloRicette : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ModelloRicette(QObject *parent = 0);

    enum SingolaRicetta{
        NomeRicetta=Qt::UserRole+1,
        ImgRicetta,
        Durata,
        Temperatura,
        Potenza,
        PotCieloPlatea
    } ;
    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QHash<int, QByteArray> roleNames() const;

private:
    QList<Ricetta>  m_ricette;
    // QAbstractItemModel interface
};

#endif // MODELLORICETTE_H
