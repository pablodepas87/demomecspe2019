#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QObject>
#include <QString>
#include <QtGui>



class Translator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString emptyString READ getEmptyString NOTIFY languageChanged)
    Q_PROPERTY(QString locale READ getLocale WRITE setLocale NOTIFY localeChanged)

public:
    explicit Translator(QObject *parent=0);
    static Translator * myTranslator;
    QString getEmptyString();

    Q_INVOKABLE void selectLanguage(quint8 language);

    Q_ENUMS(Lingue)
    enum Lingue{
        IT,
        EN,
        DE
    };


    Q_INVOKABLE QString getLocale() const;
public slots:
    void setLocale(QString valLocale);

signals:
    void languageChanged();

    void localeChanged();

private:
    QTranslator *translator1;
    QTranslator *translator2;
    QTranslator *translator3;
    QTranslator *translator4;
    QTranslator *translator5;
    QString m_locale;
};

#endif // TRANSLATOR_H
