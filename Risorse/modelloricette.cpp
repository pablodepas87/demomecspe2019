#include "modelloricette.h"
#include <QDebug>
Ricetta::Ricetta(const QString &nome, const QString &urlImg, const QTime &durata, const qint16 temperatura, const qint16 potenza, const qint16 potCieloPlatea):
                                                                                                                                                                 m_nome(nome)
                                                                                                                                                                 ,m_url(urlImg)
                                                                                                                                                                 ,m_durata(durata)
                                                                                                                                                                 ,m_temp(temperatura)
                                                                                                                                                                 ,m_potenza(potenza)
                                                                                                                                                                 ,m_potCieloPlatea(potCieloPlatea)

{
}

QString Ricetta::nome() const
{
    return m_nome;
}

QString Ricetta::url() const
{
    return m_url;
}

QTime Ricetta::durata() const
{
    return m_durata;
}

qint16 Ricetta::temp() const
{
    return m_temp;
}

qint16 Ricetta::potenza() const
{
    return m_potenza;
}

qint16 Ricetta::potCieloPlatea() const
{
    return m_potCieloPlatea;
}


ModelloRicette::ModelloRicette(QObject *parent) : QAbstractListModel(parent)
{
    m_ricette.append(Ricetta(QString(tr("Baguette")) ,"qrc:/Images/P6.jpg", QTime(0,30,0,0),200,30,70));
    m_ricette.append(Ricetta(QString(tr("Pane")),"qrc:/Images/P7.jpg", QTime(1,15,0,0),210,30,70));
    m_ricette.append(Ricetta(QString(tr("Cornetti")) ,"qrc:/Images/P9.jpg", QTime(0,50,0,0),190,30,70));
    m_ricette.append(Ricetta(QString(tr("Panini Dolci")) ,"qrc:/Images/P10.jpg", QTime(1,45,0,0),200,30,70));
    m_ricette.append(Ricetta(QString(tr("Baguette integrale")) ,"qrc:/Images/pane.jpg", QTime(1,20,0,0),200,30,70));
    m_ricette.append(Ricetta(QString(tr("Baba")), "qrc:/Images/baba-01.jpg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Patatine")), "qrc:/Images/chipsjpg.jpg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Frutta")), "qrc:/Images/fruit.jpg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Pizza piccante")), "qrc:/Images/pizza.jpg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Dolci")), "qrc:/Images/top-photo-food.jpg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Pesce")), "qrc:/Images/pesce.jpeg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Pane")), "qrc:/Images/pane.jpg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Zuppa")), "qrc:/Images/cayla1-150730-unsplash.jpg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Hamburger")), "qrc:/Images/hamburger.jpg", QTime(1,20,0,0),200,30,70) );
    m_ricette.append(Ricetta(QString(tr("Arancio")), "qrc:/Images/xiaolong-wong-1297574-unsplash.jpg", QTime(1,20,0,0),200,30,70) );
//    m_ricette.append(Ricetta(QString(""), "qrc:/.jpg", QTime(1,20,0,0),200,30,70) );
}

int ModelloRicette::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_ricette.count();
}

QVariant ModelloRicette::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    Ricetta ricTemporanea=m_ricette[index.row()];

    switch (role) {

    case NomeRicetta:

        return ricTemporanea.nome();
        break;
    case ImgRicetta:
        return ricTemporanea.url();
        break;
    case Durata:
        return ricTemporanea.durata();
        break;
    case Temperatura:
        return ricTemporanea.temp();
    case Potenza:
        return ricTemporanea.potenza();
    case PotCieloPlatea:
        return ricTemporanea.potCieloPlatea();
    default:
        break;
    }

}

bool ModelloRicette::setData(const QModelIndex &index, const QVariant &value, int role)
{
    return false;
}

QHash<int, QByteArray> ModelloRicette::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[NomeRicetta]="nomeRicetta";
    roles[ImgRicetta]="pathRicetta";
    roles[Durata]="durata";
    roles[Temperatura]="temperatura";
    roles[Potenza]="pot";
    roles[PotCieloPlatea]="potCielPlat";
    return roles;

}



