﻿#pragma once

#include <QObject>
#include <QTimer>

class StandbyEventFilter : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool abilBrightness READ abilBrightness WRITE setabilBrightness NOTIFY abilBrightnessChanged)
    Q_PROPERTY(bool standbyCottura READ standbyCottura WRITE setStandbyCottura NOTIFY standbyCotturaChanged)

public:
    StandbyEventFilter(QObject *parent = nullptr);

    Q_INVOKABLE void listenTo(QObject *object);
    Q_INVOKABLE void startTimer();
    Q_INVOKABLE void resetTimer();
    bool abilBrightness() const;
    Q_INVOKABLE void setabilBrightness(bool value);
    Q_INVOKABLE void resetBrightness();

    bool eventFilter(QObject *object, QEvent *event) override;

    bool standbyCottura() const;

public slots:
    void setStandbyCottura(bool standbyCottura);

signals:
    void timeElapsed();
    void abilBrightnessChanged();

    void standbyCotturaChanged(bool standbyCottura);

private slots:
    void goToLockScreen();
    void setBrightness(int value);
    void setTimer(int value);
    void setBrightnessStandByCottura();

private:
    QTimer timer;
    QTimer timer_stanbyCottura;
    bool m_abilBrightness;
    int m_timeToLock;
    int m_brightness;
    bool m_standbyCottura;
};
