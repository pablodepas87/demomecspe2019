#ifndef DATAORA_HPP
#define DATAORA_HPP

#include <QObject>
#include <QTimer>
#include <QDateTime>
#include <qdebug.h>
#include <QKeyEvent>
#include <QProcess>
#include <QFile>
#include <QDir>

class dataOra : public QObject
{
    Q_OBJECT
public:
    explicit dataOra(QObject *parent = nullptr);

    QTime dataCorrente();
    Q_PROPERTY(QDateTime dataoraAttuale READ dataoraAttuale WRITE setDataoraAttuale NOTIFY dataoraAttualeChanged )

    Q_INVOKABLE void setMese(QString m);
    Q_INVOKABLE QString getmese();
    Q_INVOKABLE void setDay( QString d);
    Q_INVOKABLE QString getday();
    Q_INVOKABLE void setYear(QString y);
    Q_INVOKABLE QString getyear();
    Q_INVOKABLE void setHours(QString h);
    Q_INVOKABLE QString gethours();
    Q_INVOKABLE void setMin(QString mi);
    Q_INVOKABLE QString getminuti();
    Q_INVOKABLE void setDateHours ();
    Q_INVOKABLE void setLight(int value);

signals:

    void dataoraAttualeChanged();
    void statoOraLegaleChanged(void);

public slots:
    void aggiornaData();

    QDateTime dataoraAttuale() const;
    void setDataoraAttuale(QDateTime dataoraAttuale);

private:

    QTimer *m_timer;
    QTimer *m_timerOraLegale;
    QDateTime m_dataoraAttuale;

    QString mm;
    bool var;
    QString dd;
    QString yy;
    QString hh;
    QString min;
    QDateTime m_dataOra;
    bool m_statoOraLegale;
    int m_brightness;


};

#endif // DATAORA_HPP
