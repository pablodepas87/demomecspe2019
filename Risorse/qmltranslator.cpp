#include <QGuiApplication>
#include "qmltranslator.h"
#include <QDebug>

QmlTranslator::QmlTranslator(QObject *parent) : QObject(parent)
{
    currentLang = "it_IT";
}

void QmlTranslator::setTranslation(QString translation)
{
    m_translator.load(translation + ".qm", ":/Traduzioni/");     //  nome del file .qm + path del file
//    m_translator.lo
    qApp->installTranslator(&m_translator);              // Устанавливаем его в приложение
    emit languageChanged();                              // Сигнализируем об изменении текущего перевода
    qDebug()<<translation;
    currentLang = translation;
}

void QmlTranslator::removeTranslation()
{
    qApp->removeTranslator(&m_translator);                 // Устанавливаем его в приложение
    emit languageChanged();                                 // Сигнализируем об изменении текущего перевода
}


QString QmlTranslator::getEmptyString() {
   return "";

}

QString QmlTranslator::getTranslation() {
    return currentLang;
}
