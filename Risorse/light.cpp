#include "standbyeventfilter.hpp"
#include "gestionepagine.h"
#include "parametrisetup.h"

#include <QKeyEvent>
#include <QProcess>
#include <QFile>
#include <QDir>
#include <QDebug>

//constexpr short unsigned m_timeToLock = 20000;

StandbyEventFilter::StandbyEventFilter(QObject *parent)
    : QObject(parent)
    , m_abilBrightness(false)
    , m_brightness(0)
    , m_timeToLock(0)
    , m_standbyCottura(false)
{
    m_timeToLock = ParametriSetup::m_parametriSetup->getValue(ParametriSetup::SETUP_TIMELAMP);
   // if (m_timeToLock && m_timeToLock < 10)
   //     m_timeToLock = 10;
    m_timeToLock *= 1000;
    timer_stanbyCottura.setSingleShot(true);

    setBrightness(7);

    connect(&timer, &QTimer::timeout, this, &StandbyEventFilter::goToLockScreen);
    connect(&timer_stanbyCottura, &QTimer::timeout, this, &StandbyEventFilter::setBrightnessStandByCottura);
    connect(ParametriSetup::m_parametriSetup, &ParametriSetup::timeLampChanged, this, &StandbyEventFilter::setTimer);
}

void StandbyEventFilter::listenTo(QObject *object)
{
    if (!object) {
        return;
    }
    object->installEventFilter(this);
}

void StandbyEventFilter::startTimer()
{
    //if (m_timeToLock)
    // timer.start(m_timeToLock);

    if(m_standbyCottura==true && m_timeToLock!=0)
      timer_stanbyCottura.start(m_timeToLock);
    else
      timer.start(60000);
}

void StandbyEventFilter::resetTimer()
{
    setBrightness(7);

    timer.stop();
    //if (m_timeToLock)
//    if(standbyCottura())
//      timer.start(m_timeToLock);
//    else
      timer.start(60000);
}

bool StandbyEventFilter::eventFilter(QObject *object, QEvent *event)
{
    (void)(object);
/*    if (event->type() == QEvent::MouseButtonDblClick ||
        event->type() == QEvent::MouseButtonPress ||
        event->type() == QEvent::MouseButtonRelease ||
        event->type() == QEvent::Wheel) {
        QMouseEvent* pMouseEvent = dynamic_cast<QMouseEvent*>(event);
        if ((pMouseEvent != nullptr) &&
            (pMouseEvent->source() == Qt::MouseEventSource::MouseEventSynthesizedBySystem)) {
            event->ignore();
            timer.stop();
            timer.start(m_timeToLock);
            return true;
        }
    }*/
/*
    if (event->type() == QEvent::TouchUpdate ||
        event->type() == QEvent::MouseButtonDblClick ||
        event->type() == QEvent::MouseButtonPress ||
        event->type() == QEvent::MouseButtonRelease ||
        event->type() == QEvent::Wheel) {
        event->ignore();
        timer.stop();
        timer.start(m_timeToLock);
        return true;
    }*/
    return false;
}

bool StandbyEventFilter::standbyCottura() const
{
    return m_standbyCottura;
}

void StandbyEventFilter::setStandbyCottura(bool standbyCottura)
{
    if (m_standbyCottura == standbyCottura)
        return;

    m_standbyCottura = standbyCottura;
    emit standbyCotturaChanged(m_standbyCottura);
}

void StandbyEventFilter::goToLockScreen()
{
    if (m_abilBrightness)
        setBrightness(2);

    emit timeElapsed();
}

void StandbyEventFilter::setBrightness(int value)
{
#ifdef Q_PROCESSOR_ARM
    QFile BrightnessFile("/opt/Tagliavini/Brightness.cfg") ; // se il file non esist viene creato

    if (m_brightness == value)
        return;

    m_brightness = value;

    if (BrightnessFile.exists()) {
        QProcess *qProc = new QProcess(this);

        QString set = QString("/opt/Tagliavini/Brightness.cfg %1").arg(value);  // vado a scrivere nel file che da il comando echo x cambio luminosità
        qProc->start("sh",QStringList()<< "-c"<< set);
        qProc->waitForFinished();
        qProc->close();

//        QProcess *syncProc = new QProcess(this);
//        set = QString("sync");                                                             // lancio sync altrimenti la prima volta non viene eseguito il comando Linux ha i suoi tempi
//        syncProc->start(set);
//        syncProc->waitForFinished();
//        syncProc->close();
    }
    else {
        if (!BrightnessFile.open(QIODevice::WriteOnly | QFile::Text)) {
            qDebug() << "Impossibile Aprire file per la scrittura";
            return;
        }
        else {
            QTextStream out(&BrightnessFile);
            out << "echo $1 > /sys/class/backlight/backlight/brightness";                       // scrivo il comando per modificare la luminosità
            BrightnessFile.flush();
            BrightnessFile.setPermissions(QFile::ReadOther|QFile::WriteOther|QFile::ExeOwner) ;  // equivalente del chmod 777 da linea di comando
            BrightnessFile.close();

            QProcess *syncProc = new QProcess(this);
            QString set = QString("sync");                                                             // lancio sync altrimenti la prima volta non viene eseguito il comando Linux ha i suoi tempi
            syncProc->start("sh",QStringList()<< "-c"<< set);
            syncProc->waitForFinished();
            syncProc->close();

            QProcess *qProc = new QProcess(this);
            set = QString("/opt/Tagliavini/Brightness.cfg %1").arg(value);  // vado a scrivere nel file che da il comando echo x cambio luminosità
            qProc->start(set);
            qProc->waitForFinished();
            qProc->close();

//            set = QString("sync");                                                             // lancio sync altrimenti la prima volta non viene eseguito il comando Linux ha i suoi tempi
//            syncProc->start(set);
//            syncProc->waitForFinished();
//            syncProc->close();
        }
    }
#else
    value = 0;
#endif
}

bool StandbyEventFilter::abilBrightness() const
{
    return m_abilBrightness;
}

void StandbyEventFilter::setabilBrightness(bool value)
{
    if (m_abilBrightness == value)
        return;

    m_abilBrightness = value;

    emit abilBrightnessChanged();
}

void StandbyEventFilter::resetBrightness()
{
     setBrightness(7);
}

void StandbyEventFilter::setTimer(int value)
{
    if (m_timeToLock != value) {
        m_timeToLock = value;

        resetTimer();
    }
}

void StandbyEventFilter::setBrightnessStandByCottura()
{
    setBrightness(2);
}

