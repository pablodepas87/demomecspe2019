#include "dataora.hpp"

dataOra::dataOra(QObject *parent) : QObject(parent)
{

    mm="01";
    var=false;
    dd="01";
    yy="2000";
    hh="00";
    min="00";


    m_timer = new QTimer();
    m_timer->setInterval(1000);
    m_timer->start();


    connect(m_timer, SIGNAL(timeout()), this, SLOT(aggiornaData()));
}

QDateTime dataOra::dataoraAttuale() const
{
    return m_dataoraAttuale;
}

void dataOra::setDataoraAttuale(QDateTime dataoraAttuale)
{
    if(m_dataoraAttuale != dataoraAttuale)
        m_dataoraAttuale=dataoraAttuale;
    emit dataoraAttualeChanged();
    //qDebug()<<m_dataoraAttuale;
}

void dataOra::aggiornaData()
{
    setDataoraAttuale(QDateTime::currentDateTime());
}


void dataOra::setMese(QString m){

    if (m != mm) {
        mm = m;
    }
}

QString dataOra::getmese(){
    return mm;
}

void dataOra::setDay(QString d){

    if (d != dd) {
        dd = d;
    }
}

QString dataOra::getday(){
    return dd;
}

void dataOra::setYear(QString y){
    if (y != yy) {
        yy = y;
    }
}

QString dataOra::getyear() {
    return yy;
}

void dataOra::setHours(QString h){

    if (h != hh) {
        hh = h;
    }
}
QString dataOra::gethours(){

    return hh;
}
void dataOra::setMin(QString mi){

    if (mi != min) {
        min = mi;
    }
}
QString dataOra::getminuti(){

    return min;
}

void dataOra::setDateHours()
{

    //QString command = "date -u 122314322012.00";
    QString command = "date -u "+mm+dd+hh+min+yy+".00";
    system(command.toStdString().c_str());
    command = "hwclock -w "+min+hh;
    system(command.toStdString().c_str());

    /* 2° metodo set HW CLOK
       QProcess *p = new QProcess();
      // Change system date and time "date -u MMDDhhmmYYYY.ss" in tha application the date is created dynamically
      //p->start("date -u 121711562012.00");
      p->start("date -u "+dH.getmese()+dH.getday()+dH.gethours()+dH.getminuti()+dH.getyear()+".00");
      p->waitForFinished();
      // Set the Hardware Clock to the current System Time.
      p->start("hwclock -w");
      p->waitForFinished();*/
}

void dataOra::setLight(int value) {

#ifdef Q_PROCESSOR_ARM
    QFile BrightnessFile("/opt/Tagliavini/Brightness.cfg") ; // se il file non esist viene creato

    if (m_brightness == value)
        return;

    m_brightness = value;

    if (BrightnessFile.exists()) {
        QProcess *qProc = new QProcess(this);

        QString set = QString("/opt/Tagliavini/Brightness.cfg %1").arg(value);  // vado a scrivere nel file che da il comando echo x cambio luminosità
        qProc->start("sh",QStringList()<< "-c"<< set);
        qProc->waitForFinished();
        qProc->close();

        //        QProcess *syncProc = new QProcess(this);
        //        set = QString("sync");                                                             // lancio sync altrimenti la prima volta non viene eseguito il comando Linux ha i suoi tempi
        //        syncProc->start(set);
        //        syncProc->waitForFinished();
        //        syncProc->close();
    }
    else {
        if (!BrightnessFile.open(QIODevice::WriteOnly | QFile::Text)) {
            qDebug() << "Impossibile Aprire file per la scrittura";
            return;
        }
        else {
            QTextStream out(&BrightnessFile);
            out << "echo $1 > /sys/class/backlight/backlight/brightness";                       // scrivo il comando per modificare la luminosità
            BrightnessFile.flush();
            BrightnessFile.setPermissions(QFile::ReadOther|QFile::WriteOther|QFile::ExeOwner) ;  // equivalente del chmod 777 da linea di comando
            BrightnessFile.close();

            QProcess *syncProc = new QProcess(this);
            QString set = QString("sync");                                                             // lancio sync altrimenti la prima volta non viene eseguito il comando Linux ha i suoi tempi
            syncProc->start("sh",QStringList()<< "-c"<< set);
            syncProc->waitForFinished();
            syncProc->close();

            QProcess *qProc = new QProcess(this);
            set = QString("/opt/Tagliavini/Brightness.cfg %1").arg(value);  // vado a scrivere nel file che da il comando echo x cambio luminosità
            qProc->start(set);
            qProc->waitForFinished();
            qProc->close();

            //            set = QString("sync");                                                             // lancio sync altrimenti la prima volta non viene eseguito il comando Linux ha i suoi tempi
            //            syncProc->start(set);
            //            syncProc->waitForFinished();
            //            syncProc->close();
        }
    }
#else
    value = 0;
#endif
}




