#ifndef FILTRO_H
#define FILTRO_H
#include <QSortFilterProxyModel>

class Filtro : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    Filtro(QObject* parent=0);
    ~Filtro();

    Q_INVOKABLE void setStringaFiltro(QString string);
    Q_PROPERTY(QString string READ string WRITE setstring NOTIFY stringChanged)

public slots:




    QString string() const;
    void setstring(QString string);

signals:


    void stringChanged();

private:

    QString m_string;

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;


    // QSortFilterProxyModel interface

};

#endif // FILTRO_H
