#include "filtro.h"
#include "modelloricette.h"
#include <QDebug>

Filtro::Filtro(QObject *parent): QSortFilterProxyModel(parent)
                                  ,m_string("")
{
    setFilterRole(ModelloRicette::NomeRicetta);
    //setSortRole(modello::NomeRicetta); // se attivo vengono ordinati in ordine alfabetico
    setDynamicSortFilter(false);
    // sort(0);
}

Filtro::~Filtro()
{

}

void Filtro::setStringaFiltro(QString string)
{
    this->setFilterCaseSensitivity(Qt::CaseInsensitive); // lo rendo case insensitive
    this->setFilterFixedString(string);

}

QString Filtro::string() const
{
    return m_string;
}

void Filtro::setstring(QString string)
{
    if (m_string == string)
        return;

    m_string = string;
    emit stringChanged();
    invalidateFilter();  // fa rivalutare il filtro e quindi entra di nuovo in filterAcceptsRows()
}

bool Filtro::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{


    QRegExp regEx("*"+string()+"*");
    regEx.setPatternSyntax(QRegExp::Wildcard);
    regEx.setCaseSensitivity(Qt::CaseInsensitive);



    QModelIndex ricUtente = sourceModel()->index(source_row,0, source_parent); // vado a leggere singolarmente ogni riga del modello
    QString stringaConfronto=sourceModel()->data(ricUtente,ModelloRicette::NomeRicetta).toString();
    if(stringaConfronto.contains(regEx))
        return true;

    return false;


}




