#include "translator.h"
#include <QDebug>

Translator * Translator::myTranslator = nullptr;

Translator::Translator(QObject *parent):QObject(parent)
  , m_locale("it_IT")
{
    translator1 = new QTranslator(this);
    translator2 = new QTranslator(this);
}

QString Translator::getEmptyString() {
    return "";
}

void Translator::selectLanguage(quint8 language) {


    qDebug()<<"Sono in select language";

    if(language == Translator::EN) {
        if (translator1->load(":/Traduzioni/en_US.ts", "Traduzioni"))
        qApp->installTranslator(translator1);
        setLocale("en_US");
        qDebug()<<"en us";
    }

    if(language == Translator::DE) {
        translator2->load(":/Traduzioni/de_DE.ts", "Traduzioni");
        qApp->installTranslator(translator2);
        setLocale("de_DE");
        qDebug()<<"de de";
    }

    if(language == Translator::IT) {
        qApp->removeTranslator(translator1);
        qApp->removeTranslator(translator2);
        setLocale("it_IT");
        qDebug()<<"it it2";
    }

    emit languageChanged();
}

QString Translator::getLocale() const
{
    return m_locale;
}

void Translator::setLocale(QString valLocale)
{
    if (m_locale == valLocale)
        return;

    m_locale = valLocale;
    emit localeChanged();
}
