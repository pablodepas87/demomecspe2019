import QtQuick 2.0

Text {
    Caratteri {
        id: caratteri
    }

    text: "\uf080"
    color: "white"
    font.family: caratteri.awesome
    font.pixelSize: 60
    anchors.fill: parent
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
}
