import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Text {
    verticalAlignment: Text.AlignVCenter
    color: colori.verde
    fontSizeMode: Text.Fit
    font.family: caratteri.kl_Bold
    height: parent.height
}
