import QtQuick 2.0
import QtQuick.Controls 2.0

Button {

    id:root
    Caratteri {
        id: caratteri
    }
    Colori{
        id: colori
    }
    x: 1100
    width: 140
    height: 40
    background: Rectangle{
        color: colori.verde
        border.width:2
        radius: 5
        border.color: colori.grigio
    }

    contentItem: Text{
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 20
        font.family: caratteri.kl_Regular
        text: root.text
    }
}
