import QtQuick 2.9
import "../Font"

Item {

    property alias kl_Bold: klBold.name
    property alias kl_Medium: klMedium.name
    property alias kl_Regular: klRegular.name
    property alias awesome: awesome.name
    property alias cookie: cookie.name

    FontLoader {
        id: cookie
        source: "qrc:/Font/Cookie-Regular.ttf"
    }

    FontLoader {
        id: awesome
        source: "qrc:/Font/fontawesome-webfont.ttf"
    }

    FontLoader {
        id: klBold
//                    name: "Klavika Bold Italic"
        source: "qrc:/Font/Klavika Bold Italic.otf"
    }
    FontLoader {
        id: klMedium
//                    name: "Klavika Medium"
        source: "qrc:/Font/Klavika Medium.otf"
    }
    FontLoader {
        id: klRegular
//                    name: "Klavika Regular"
        source: "qrc:/Font/Klavika Regular.otf"
    }
}

