import QtQuick 2.9
import QtQuick.Controls 2.0

import "Graphs"

import QtCharts 2.0

Item {
    width: 1220
    height: 650
    property bool sourceLoaded: false
    property alias currentIndex: root.currentIndex

    anchors.centerIn: parent

    ListView {
        id: root
        focus: true
        anchors.fill: parent
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: 250
        orientation: ListView.Horizontal
        boundsBehavior: Flickable.StopAtBounds

        model: ListModel {
            ListElement {
                component: "qrc:/Graphs/View1.qml"
            }
            ListElement {
                component: "qrc:/Graphs/View12.qml"
            }
            ListElement {
                component: "qrc:/Graphs/View2.qml"
            }
            ListElement {
                component: "qrc:/Graphs/View4.qml"
            }
            ListElement {
                component: "qrc:/Graphs/View5.qml"
            }
            ListElement {
                component: "qrc:/Graphs/View7.qml"
            }
            ListElement {
                component: "qrc:/Graphs/View9.qml"
            }
        }

        delegate: Loader {
            width: root.width
            height: root.height

            source: component
            asynchronous: true

            onLoaded: sourceLoaded = true
        }
    }

    PageIndicator {
        currentIndex: root.currentIndex
        count: root.count
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
