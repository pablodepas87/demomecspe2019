<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>Calendario</name>
    <message>
        <location filename="../Calendario.qml" line="47"/>
        <source>SALVA</source>
        <translation>SALVA</translation>
    </message>
    <message>
        <location filename="../Calendario.qml" line="117"/>
        <source>IMPOSTA ORA</source>
        <translation>IMPOSTA ORA</translation>
    </message>
    <message>
        <location filename="../Calendario.qml" line="253"/>
        <source>IMPOSTA DATA</source>
        <translation>IMPOSTA DATA</translation>
    </message>
</context>
<context>
    <name>HomeForm.ui</name>
    <message>
        <location filename="../HomeForm.ui.qml" line="10"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
</context>
<context>
    <name>Impostazioni</name>
    <message>
        <location filename="../Impostazioni.qml" line="26"/>
        <source>seleziona la lingua: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Impostazioni.qml" line="126"/>
        <location filename="../Impostazioni.qml" line="140"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LiveChart</name>
    <message>
        <location filename="../LiveChart.qml" line="18"/>
        <source>Serie animate</source>
        <translation>Serie animate</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="156"/>
        <source>destra ---&gt;</source>
        <translation>destra ---&gt;</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="165"/>
        <source>sinistra &lt;---</source>
        <translation>sinistra &lt;---</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="174"/>
        <source>reset</source>
        <translation>reset</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="190"/>
        <source>zoom +</source>
        <translation>zoom +</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="203"/>
        <source>zoom -</source>
        <translation>zoom -</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="214"/>
        <source>zoom 0</source>
        <translation>zoom 0</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="238"/>
        <source>Serie 1</source>
        <translation>Serie 1</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="251"/>
        <source>Serie 2</source>
        <translation>Serie 2</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="264"/>
        <source>Serie 3</source>
        <translation>Serie 3</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="277"/>
        <source>Serie 4</source>
        <translation>Serie 4</translation>
    </message>
</context>
<context>
    <name>ModelloRicette</name>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="47"/>
        <source>Baguette</source>
        <translation>Baguette</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="48"/>
        <location filename="../Risorse/modelloricette.cpp" line="58"/>
        <source>Pane</source>
        <translation>Pane</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="49"/>
        <source>Cornetti</source>
        <translation>Cornetti</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="50"/>
        <source>Panini Dolci</source>
        <translation>Panini Dolci</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="51"/>
        <source>Baguette integrale</source>
        <translation>Baguette integrale</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="52"/>
        <source>Baba</source>
        <translation>Baba</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="53"/>
        <source>Patatine</source>
        <translation>Patatine</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="54"/>
        <source>Frutta</source>
        <translation>Frutta</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="55"/>
        <source>Pizza piccante</source>
        <translation>Pizza piccante</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="56"/>
        <source>Dolci</source>
        <translation>Dolci</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="57"/>
        <source>Pesce</source>
        <translation>Pesce</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="59"/>
        <source>Zuppa</source>
        <translation>Zuppa</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="60"/>
        <source>Hamburger</source>
        <translation>Hamburger</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="61"/>
        <source>Arancio</source>
        <translation>Arancio</translation>
    </message>
</context>
<context>
    <name>Page1Form</name>
    <message>
        <location filename="../Page1Form.qml" line="63"/>
        <source>Grafici</source>
        <translation>Grafici</translation>
    </message>
    <message>
        <location filename="../Page1Form.qml" line="113"/>
        <source>Grafici animati</source>
        <translation>Grafici animati</translation>
    </message>
</context>
<context>
    <name>Page2Form.ui</name>
    <message>
        <location filename="../Page2Form.ui.qml" line="38"/>
        <source>Cerca...</source>
        <translation>Cerca...</translation>
    </message>
</context>
<context>
    <name>Ricette</name>
    <message>
        <location filename="../Ricette.qml" line="24"/>
        <source>Lista Ricette</source>
        <translation>Lista Ricette</translation>
    </message>
    <message>
        <location filename="../Ricette.qml" line="74"/>
        <source>Cerca Ricetta</source>
        <translation>Cerca Ricetta</translation>
    </message>
</context>
<context>
    <name>View1</name>
    <message>
        <location filename="../Graphs/View1.qml" line="17"/>
        <source>grafico a barre</source>
        <translation>grafico a barre</translation>
    </message>
</context>
<context>
    <name>View12</name>
    <message>
        <location filename="../Graphs/View12.qml" line="13"/>
        <source>Top-5 marchi automobilistici</source>
        <translation>Top-5 marchi automobilistici</translation>
    </message>
    <message>
        <location filename="../Graphs/View12.qml" line="41"/>
        <source>Antialiasing</source>
        <translation>Antialiasing</translation>
    </message>
    <message>
        <location filename="../Graphs/View12.qml" line="50"/>
        <source>Stacca</source>
        <translation>Stacca</translation>
    </message>
    <message>
        <location filename="../Graphs/View12.qml" line="59"/>
        <source>Aggiungi</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <location filename="../Graphs/View12.qml" line="63"/>
        <source>Altri</source>
        <translation>Altri</translation>
    </message>
</context>
<context>
    <name>View2</name>
    <message>
        <location filename="../Graphs/View2.qml" line="8"/>
        <source>linea</source>
        <translation>linea</translation>
    </message>
    <message>
        <location filename="../Graphs/View2.qml" line="13"/>
        <source>Grafico a linee</source>
        <translation>Grafico a linee</translation>
    </message>
</context>
<context>
    <name>View4</name>
    <message>
        <location filename="../Graphs/View4.qml" line="12"/>
        <source>Rosso</source>
        <translation>Rosso</translation>
    </message>
    <message>
        <location filename="../Graphs/View4.qml" line="13"/>
        <source>Giallo</source>
        <translation>Giallo</translation>
    </message>
    <message>
        <location filename="../Graphs/View4.qml" line="14"/>
        <source>Verde</source>
        <translation>Verde</translation>
    </message>
    <message>
        <location filename="../Graphs/View4.qml" line="17"/>
        <source>Grafico ad aree</source>
        <translation>Grafico ad aree</translation>
    </message>
</context>
<context>
    <name>View5</name>
    <message>
        <location filename="../Graphs/View5.qml" line="10"/>
        <source>Scatter</source>
        <translation>Scatter</translation>
    </message>
    <message>
        <location filename="../Graphs/View5.qml" line="46"/>
        <source>Mostra Scatter 2</source>
        <translation>Mostra Scatter 2</translation>
    </message>
</context>
<context>
    <name>View7</name>
    <message>
        <location filename="../Graphs/View7.qml" line="8"/>
        <source>Grafico a barre orizzontali</source>
        <translation>Grafico a barre orizzontali</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="26"/>
        <source>Stack</source>
        <translation>Stack</translation>
    </message>
    <message>
        <location filename="../main.qml" line="44"/>
        <location filename="../main.qml" line="52"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../main.qml" line="199"/>
        <source>Grafici</source>
        <translation>Grafici</translation>
    </message>
    <message>
        <location filename="../main.qml" line="200"/>
        <source>Tastiera virtuale</source>
        <translation>Tastiera virtuale</translation>
    </message>
    <message>
        <location filename="../main.qml" line="201"/>
        <source>Pdf</source>
        <translation>Pdf</translation>
    </message>
    <message>
        <location filename="../main.qml" line="202"/>
        <source>Impostazioni</source>
        <translation>Impostazioni</translation>
    </message>
</context>
</TS>
