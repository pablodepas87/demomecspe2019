<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Calendario</name>
    <message>
        <location filename="../Calendario.qml" line="47"/>
        <source>SALVA</source>
        <translation>SAVE</translation>
    </message>
    <message>
        <location filename="../Calendario.qml" line="117"/>
        <source>IMPOSTA ORA</source>
        <translation>SET HOUR</translation>
    </message>
    <message>
        <location filename="../Calendario.qml" line="253"/>
        <source>IMPOSTA DATA</source>
        <translation>SET DATE</translation>
    </message>
</context>
<context>
    <name>HomeForm.ui</name>
    <message>
        <location filename="../HomeForm.ui.qml" line="10"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
</context>
<context>
    <name>Impostazioni</name>
    <message>
        <location filename="../Impostazioni.qml" line="26"/>
        <source>seleziona la lingua: </source>
        <translation>Select language:</translation>
    </message>
    <message>
        <location filename="../Impostazioni.qml" line="126"/>
        <location filename="../Impostazioni.qml" line="140"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LiveChart</name>
    <message>
        <location filename="../LiveChart.qml" line="18"/>
        <source>Serie animate</source>
        <translation>Animated series</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="156"/>
        <source>destra ---&gt;</source>
        <translation>right--&gt;</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="165"/>
        <source>sinistra &lt;---</source>
        <translation>left&lt;---</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="174"/>
        <source>reset</source>
        <translation>reset</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="190"/>
        <source>zoom +</source>
        <translation>zoom+</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="203"/>
        <source>zoom -</source>
        <translation>zoom-</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="214"/>
        <source>zoom 0</source>
        <translation>zoom0</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="238"/>
        <source>Serie 1</source>
        <translation>1st series</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="251"/>
        <source>Serie 2</source>
        <translation>2nd series</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="264"/>
        <source>Serie 3</source>
        <translation>3rd series</translation>
    </message>
    <message>
        <location filename="../LiveChart.qml" line="277"/>
        <source>Serie 4</source>
        <translation>4th series</translation>
    </message>
</context>
<context>
    <name>ModelloRicette</name>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="47"/>
        <source>Baguette</source>
        <translation>Baguette</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="48"/>
        <location filename="../Risorse/modelloricette.cpp" line="58"/>
        <source>Pane</source>
        <translation>Bread</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="49"/>
        <source>Cornetti</source>
        <translation>Croissant</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="50"/>
        <source>Panini Dolci</source>
        <translation>Bread and desserts</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="51"/>
        <source>Baguette integrale</source>
        <translation>Integral Baguette</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="52"/>
        <source>Baba</source>
        <translation>Baba</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="53"/>
        <source>Patatine</source>
        <translation>Chips</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="54"/>
        <source>Frutta</source>
        <translation>Fruit</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="55"/>
        <source>Pizza piccante</source>
        <translation>Pizza</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="56"/>
        <source>Dolci</source>
        <translation>Desserts</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="57"/>
        <source>Pesce</source>
        <translation>Fish</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="59"/>
        <source>Zuppa</source>
        <translation>Soup</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="60"/>
        <source>Hamburger</source>
        <translation>Hamburger</translation>
    </message>
    <message>
        <location filename="../Risorse/modelloricette.cpp" line="61"/>
        <source>Arancio</source>
        <translation>orange</translation>
    </message>
</context>
<context>
    <name>Page1Form</name>
    <message>
        <location filename="../Page1Form.qml" line="63"/>
        <source>Grafici</source>
        <translation>Charts</translation>
    </message>
    <message>
        <location filename="../Page1Form.qml" line="113"/>
        <source>Grafici animati</source>
        <translation>Live charts</translation>
    </message>
</context>
<context>
    <name>Page2Form.ui</name>
    <message>
        <location filename="../Page2Form.ui.qml" line="38"/>
        <source>Cerca...</source>
        <translation>Search...</translation>
    </message>
</context>
<context>
    <name>Ricette</name>
    <message>
        <location filename="../Ricette.qml" line="24"/>
        <source>Lista Ricette</source>
        <translation>Recipe list</translation>
    </message>
    <message>
        <location filename="../Ricette.qml" line="74"/>
        <source>Cerca Ricetta</source>
        <translation>Search Recipe</translation>
    </message>
</context>
<context>
    <name>View1</name>
    <message>
        <location filename="../Graphs/View1.qml" line="17"/>
        <source>grafico a barre</source>
        <translation>bars chart</translation>
    </message>
</context>
<context>
    <name>View12</name>
    <message>
        <location filename="../Graphs/View12.qml" line="13"/>
        <source>Top-5 marchi automobilistici</source>
        <translation>Top 5 car brands</translation>
    </message>
    <message>
        <location filename="../Graphs/View12.qml" line="41"/>
        <source>Antialiasing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Graphs/View12.qml" line="50"/>
        <source>Stacca</source>
        <translation>Explode</translation>
    </message>
    <message>
        <location filename="../Graphs/View12.qml" line="59"/>
        <source>Aggiungi</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../Graphs/View12.qml" line="63"/>
        <source>Altri</source>
        <translation>Others</translation>
    </message>
</context>
<context>
    <name>View2</name>
    <message>
        <location filename="../Graphs/View2.qml" line="8"/>
        <source>linea</source>
        <translation>Line</translation>
    </message>
    <message>
        <location filename="../Graphs/View2.qml" line="13"/>
        <source>Grafico a linee</source>
        <translation>Lines Chart</translation>
    </message>
</context>
<context>
    <name>View4</name>
    <message>
        <location filename="../Graphs/View4.qml" line="12"/>
        <source>Rosso</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../Graphs/View4.qml" line="13"/>
        <source>Giallo</source>
        <translation>Yellow</translation>
    </message>
    <message>
        <location filename="../Graphs/View4.qml" line="14"/>
        <source>Verde</source>
        <translation>Green</translation>
    </message>
    <message>
        <location filename="../Graphs/View4.qml" line="17"/>
        <source>Grafico ad aree</source>
        <translation>Area chart</translation>
    </message>
</context>
<context>
    <name>View5</name>
    <message>
        <location filename="../Graphs/View5.qml" line="10"/>
        <source>Scatter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Graphs/View5.qml" line="46"/>
        <source>Mostra Scatter 2</source>
        <translation>Show Scatter 2</translation>
    </message>
</context>
<context>
    <name>View7</name>
    <message>
        <location filename="../Graphs/View7.qml" line="8"/>
        <source>Grafico a barre orizzontali</source>
        <translation>Horizontal bars chart</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="26"/>
        <source>Stack</source>
        <translation>Stack</translation>
    </message>
    <message>
        <location filename="../main.qml" line="44"/>
        <location filename="../main.qml" line="52"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../main.qml" line="199"/>
        <source>Grafici</source>
        <translation>Charts</translation>
    </message>
    <message>
        <location filename="../main.qml" line="200"/>
        <source>Tastiera virtuale</source>
        <translation>Virtual keyboard</translation>
    </message>
    <message>
        <location filename="../main.qml" line="201"/>
        <source>Pdf</source>
        <translation>Pdf</translation>
    </message>
    <message>
        <location filename="../main.qml" line="202"/>
        <source>Impostazioni</source>
        <translation>Settings</translation>
    </message>
</context>
</TS>
