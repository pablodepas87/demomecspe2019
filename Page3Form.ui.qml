import QtQuick 2.4
import QtQuick.Controls 2.0

Page {
    id: pagina

    Loader {
        source: "qrc:/Impostazioni.qml"
        width: 800
        height: 600

        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
