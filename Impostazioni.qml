import QtQuick 2.9
import QtQuick.Controls 2.0
import QtMultimedia 5.9
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import "Globali"

Item {
    property string lingua: myTranslator.getTranslation()
    property bool primo: false
    property int margini: 50

//    anchors.fill: parent    

    id: tutto
    Colori {
        id: colori
    }
    Caratteri {
        id: caratteri
    }

    ColumnLayout {
        Text {
            text: qsTr("seleziona la lingua: ")+myTranslator.emptyString
            font.family: caratteri.kl_Regular
            font.pixelSize: 26

            Layout.topMargin: margini
            Layout.leftMargin: margini
        }
        RowLayout {
            Layout.topMargin: margini
            Layout.leftMargin: margini
            spacing: margini
            Rectangle {
                width: 220
                height: 220
                color: "white"
                id: bordoIt
                border.width: 3
                border.color: lingua == "it_IT" ? colori.verde : "white"
                Image {
                    width: 200
                    height: 200
                    anchors.centerIn: parent
                    source: "qrc:/Images/it.png"
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (lingua != "it_IT") {
                                lingua = "it_IT"

                                myTranslator.setTranslation("it_IT");
                            }
                        }
                    }
                }
            }
            Rectangle {
                width: 220
                height: 220
                color: "white"
                id: bordoEn
                border.width: 3
                border.color: lingua == "en_US" ? colori.verde : "white"
                Image {
                    width: 200
                    height: 200
                    anchors.centerIn: parent
                    source: "qrc:/Images/en.png"
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (lingua != "en_US") {
                                lingua = "en_US"

                                myTranslator.setTranslation("en_US");
                            }
                        }
                    }
                }
            }
            Rectangle {
                width: 220
                height: 220
                color: "white"
                id: bordoDe
                border.width: 3
                border.color: lingua == "de_DE" ? colori.verde : "white"
                Image {
                    width: 200
                    height: 200
                    anchors.centerIn: parent
                    source: "qrc:/Images/de.png"
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (lingua != "de_DE") {
                                lingua = "de_DE"

                                myTranslator.setTranslation("de_DE");
                            }
                        }
                    }
                }
            }
        } //end row


        RowLayout {
            Text {
                text: luminosita.value.toString()
                Layout.leftMargin: margini
                Layout.rightMargin: margini
                font.pixelSize: 26
                font.family: caratteri.kl_Medium
            }
            Layout.topMargin: margini
            Layout.leftMargin: margini
            Text {
                text: qsTr("\uf0eb")
                font.family: caratteri.awesome
                font.pixelSize: 30
            }
            Slider {
                id: luminosita
                implicitWidth: 400
                implicitHeight: 100
                from: 1
                value: 10
                to: 10
                stepSize: 1

                onValueChanged: {
                    myData.setLight(value)
                }
            }
            Text {
                text: qsTr("\uf0eb")
                font.family: caratteri.awesome
                font.pixelSize: 30
                //                font.weight: 99
            }
        }
    } //end column
}
