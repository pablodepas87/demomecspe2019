import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.VirtualKeyboard 2.1
import QtQuick.Layouts 1.3

import "Globali"

Page {
    id: page
    width: 1280
    height: 800

    Caratteri {
        id: caratteri
    }
    Colori {
        id: colori
    }
    Barra {
        id: barra
    }

    header: Rectangle {
        height: 30
        width: parent.width
        color: "white"
    }

    background: Rectangle {
        //                color:"#ebebeb"
        color: "white"
        //                color: colori.grigio
    }

    TextField {
        id: textField

        placeholderText: qsTr("Cerca...")+myTranslator.emptyString
        font.pointSize: 12
        color: colori.grigio
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.8
//        anchors.verticalCenter: parent.verticalCenter

        focus: false

        background: Rectangle {
            radius: 5
            width: parent.width
            implicitHeight: 30
            border.color: colori.grigio
            height: parent.height
        }
        onTextChanged: {
            //myModelFiltrato.setStringaFiltro(text)
            myModelFiltrato.string = text
        }
    }

    Loader {
        source: "qrc:/VirtualKeyboard.qml"
        asynchronous: true

//        onLoaded: sourceLoaded = true
        anchors.centerIn: parent
    }
}
